<?php

namespace Drupal\time_ticker\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\time_ticker\TimeService;
use Symfony\Component\HttpFoundation\JsonResponse;


class TimeController extends ControllerBase{

    protected $time;

      /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param Drupal\time_ticker\TimeService $time
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $killSwitch
   *  The page cache kill switch service.
   */

  public function __construct(TimeService $time) {
    $this->time = $time;
  }
   /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(   
      $container->get('time_ticker.time'),
    );
  }

    public function getTime(){
        
        return new JsonResponse($this->time->getTime());
    }

}