<?php

/**
 * @file
 * Contains the Time Ticker service.
 */

namespace Drupal\time_ticker;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Config\ConfigFactory;

class TimeService {

    /**
   * The Config Factory variable.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   */

  public function __construct(ConfigFactory $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Get current time as per selected timezone.
   *
   */
  public function getTime() { 
    
    $config = $this->configFactory->get('time_ticker.settings');
    $timezone = $config->get('timezone');

    
    $current_time = new DrupalDateTime('now','UTC');

    $userTimezone = new \DateTimeZone($timezone);

    $date_time = new DrupalDateTime();

    $timezone_offset = $userTimezone->getOffset($date_time->getPhpDateTime());

    $time_interval = \DateInterval::createFromDateString($timezone_offset . 'seconds');

    $result = $current_time->add($time_interval);

    $result = $current_time->format('jS M Y - h:i:s A');

    
    return $result; 

  }
 
}
