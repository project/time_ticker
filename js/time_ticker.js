(function($, Drupal){

    Drupal.behaviors.myBehavior = {
        attach: function (context, settings) {            
                       
            function updateTime() {
                
             $.get('/time_ticker/ajax', null, processTime);
                
            }
        setInterval(updateTime, 1000);

        }
    }
   
    processTime = function(response){
        var time = response.split('-');
        $('#date').html(time[0]);
        $('#time').html(time[1]);
    }
     
})(jQuery, Drupal);